﻿using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.ViewModel
{
	public class RecoverPasswordViewModel
	{
		[Required(ErrorMessage = "Username cannot be empty")]
		public string Username { get; set; }

		[Required(ErrorMessage = "Email cannot be empty")]
		[EmailAddress(ErrorMessage = "Invalid email")]
		public string Email { get; set; }

		public UserType TypeOfUser { get; set; }
	}
}