﻿using Autofac;
using FacultyManagement.Services;

namespace FacultyManagement.Bootstrap
{
	public class ServicesRegistrar
	{
		public static void RegisterWith(ContainerBuilder builder)
		{
			builder.RegisterType<LoginService>().SingleInstance();
			builder.RegisterType<StudentService>().SingleInstance();
			builder.RegisterType<TeacherService>().SingleInstance();
		}
	}
}