﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.ViewModel;
using FacultyManagement.Services;
using FacultyManagement.Repositories;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.StudentsGrade;
namespace FacultyManagement.Controllers
{

    public class ViewStudentController : Controller
    {
        private readonly StudentService studentService = new StudentService(new StudentRepository());
        private readonly IStudentsGradeRepository<StudentsFacultativeCourseGrade> faccultativeCoursesRepository = new StudentsFacultativeCourseGradeRepository();
        private readonly IStudentsGradeRepository<StudentsOptionalCourseGrade> optionalCoursesRepository = new StudentsOptionalCourseGradeRepository();
        private readonly IStudentsGradeRepository<StudentsMandatoryCourseGrade> mandatoryCoursesRepository = new StudentsMandatoryCourseGradeRepository();

        public List<ICourse> Courses(string username)
        {
            List<ICourse> var = new List<ICourse>();

            int id = this.studentService.GetAll().First(student => student.Username.Equals(username)).Id;
            var studentViewModel = this.studentService.FindById(id);
            if (studentViewModel == null)
            {

                return var;
            }
            else
            {
                var.Concat(mandatoryCoursesRepository.FindStudentCourse(id));
                var.Concat(optionalCoursesRepository.FindStudentCourse(id));
                var.Concat(faccultativeCoursesRepository.FindStudentCourse(id));
                return var;
            }
        }
    }
}