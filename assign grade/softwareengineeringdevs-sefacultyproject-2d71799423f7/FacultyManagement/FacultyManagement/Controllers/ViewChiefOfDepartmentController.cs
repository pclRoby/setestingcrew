﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Repositories;
using FacultyManagement.Services;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Controllers
{
    public class ViewChiefOfDepartmentController : Controller
    {
        private readonly TeacherService teacherService = new TeacherService(new TeacherRepository());

        public TeacherViewModel Details(string username)
        {
            int id = this.teacherService.GetAll().First(teacher => teacher.Username.Equals(username)).Id;
            var teacherViewModel = this.teacherService.FindById(id);
            return teacherViewModel;
        }

        public bool Create( TeacherViewModel var)
        {
            if (ModelState.IsValid)
            {
                this.teacherService.Add(var);
                return true;
            }
            return false;
        }

        public bool Edit(string username, TeacherViewModel var)
        {
            int id = this.teacherService.GetAll().First(teacher => teacher.Username.Equals(username)).Id;
            var teacherViewModel = this.teacherService.FindById(id);
            if (ModelState.IsValid)
            {
                var.Id = teacherViewModel.Id;
                var.Password = teacherViewModel.Password;

                this.teacherService.UpdateUser(var);
                return true;
            }
            return false;
        }

        public bool Delete(string username)
        {
            int id = this.teacherService.GetAll().First(teacher => teacher.Username.Equals(username)).Id;
            var teacherViewModel = this.teacherService.FindById(id);
            if (teacherViewModel == null)
            {
                return false;
            }
            else
            {
                this.teacherService.DeleteUser(id);
                return true;
            }
        }

    }
}