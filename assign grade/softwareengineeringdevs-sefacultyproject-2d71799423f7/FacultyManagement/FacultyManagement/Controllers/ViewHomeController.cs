﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.ViewModel;
using FacultyManagement.Services;
using FacultyManagement.Repositories;


namespace FacultyManagement.Controllers
{
    public class ViewHomeController : Controller
    {
        private readonly LoginService loginService = new LoginService(
            new AdminStaffRepository(),
            new TeacherRepository(),
            new CODRepository(),
            new StudentRepository()
            );

        public bool Login(LoginViewModel var)
        {
            try
            {
                return this.loginService.ValidateUser(ref var);
            }
            catch
            {
                return false;
            }
        }

        public bool RecoverPassword(RecoverPasswordViewModel var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    this.loginService.RecoverPassword(var);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool ChangePassword(ChangePasswordViewModel var)
        {
            try
            {
                var user = this.loginService.GetRepositoryUser(var.Username, var.TypeOfUser);
                if (this.loginService.IsResetCodeValid(user, var.ResetCode))
                {
                    loginService.ResetPassword(user, var.NewPassword);
                    return true;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }


    }
}