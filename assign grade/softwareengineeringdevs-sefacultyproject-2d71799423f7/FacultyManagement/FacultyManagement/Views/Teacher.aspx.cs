﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.StudentsGrade;
using FacultyManagement.Models;
using System.Data;

namespace FacultyManagement.Views
{
    public partial class Teacher : System.Web.UI.Page
    {
        private ViewTeacherController ctrl = new ViewTeacherController();
		private String username;

        protected void Page_Load(object sender, EventArgs e)
        {
			username = (string)Session["New"];
			username = username.Split('.')[1];
			LoadAllMarks();
			LoadCourseOptions();
			if (Session["New"] != null) {
				string Type = (string)Session["New"];
				Type = Type.Split('.')[0];
				if (Type == "Teacher") {
					;
				}
				else {
					Response.Redirect(Type + ".aspx");
				}
			}
			else {
				Response.Redirect("Login.aspx");
			}
        }

		protected void CourseOptionChanged(object sender,EventArgs args) {
			if (((DropDownList)sender).SelectedValue == "All Courses"){
				LoadAllMarks();
				return;
			}
			List<CourseGradeViewModel> list = ctrl.GetCourseGradesByTeacher(username, ((DropDownList)sender).SelectedValue);
			DataTable dt = new DataTable();
			dt.Columns.Add(new DataColumn("CourseID", typeof(string)));
			dt.Columns.Add(new DataColumn("CourseName", typeof(string)));
			dt.Columns.Add(new DataColumn("CourseType", typeof(string)));
			dt.Columns.Add(new DataColumn("StudentID", typeof(string)));
			dt.Columns.Add(new DataColumn("StudentName", typeof(string)));
			dt.Columns.Add(new DataColumn("Grade", typeof(int)));
			dt.Columns.Add(new DataColumn("Confirm", typeof(CheckBox)));
			courseMarks.DataSource = dt;
			foreach (CourseGradeViewModel cgrade in list) {
				DataRow dr = dt.NewRow();
				dr["CourseID"] = cgrade.Course_Id.ToString();
				dr["CourseName"] = cgrade.CourseName;
				dr["CourseType"] = cgrade.Type;
				dr["StudentID"] = cgrade.StudentId.ToString();
				dr["StudentName"] = cgrade.StudentName;
				dr["Grade"] = cgrade;
				dt.Rows.Add(dr);
			}
		}

		protected void LoadCourseOptions() {
			List<ICourse> list=ctrl.Courses(username);
			if (list.Count == 0) {
				//mesaj de eroare, profesorul nu are cursuri
				return;
			}
			courseOption.Items.Add(new ListItem("All Courses"));
			foreach (ICourse course in list) {
				courseOption.Items.Add(new ListItem(course.Name));
			}
		}

		protected void LoadAllMarks() {
			List<CourseGradeViewModel> list = ctrl.GetGradesByTeacher(username);
			DataTable dt = new DataTable();
			dt.Columns.Add(new DataColumn("CourseID", typeof(string)));
			dt.Columns.Add(new DataColumn("CourseName", typeof(string)));
			dt.Columns.Add(new DataColumn("CourseType", typeof(string)));
			dt.Columns.Add(new DataColumn("StudentID", typeof(string)));
			dt.Columns.Add(new DataColumn("StudentName", typeof(string)));
			dt.Columns.Add(new DataColumn("Grade", typeof(int)));
			dt.Columns.Add(new DataColumn("Confirm", typeof(CheckBox)));
			courseMarks.DataSource = dt;
			foreach (CourseGradeViewModel cgrade in list) {
				DataRow dr = dt.NewRow();
				dr["CourseID"] = cgrade.Course_Id.ToString();
				dr["CourseName"] = cgrade.CourseName;
				dr["CourseType"] = cgrade.Type;
				dr["StudentID"] = cgrade.StudentId.ToString();
				dr["StudentName"] = cgrade.StudentName;
				dr["Grade"] = cgrade;
				dt.Rows.Add(dr);
			}
			//Daca lista e goala afiseaza mesaj de eroare. daca tabelul e gol, atunci el nu mai e afisat
			courseMarks.DataBind();
		}

        protected void ButtonAssignMarks_Click(object sender, EventArgs e)
        {
            //in username e userul profului care e logat
            //string username = (string)Session["New"];
            //username = username.Split('.')[1];
			foreach (GridViewRow row in courseMarks.Rows) {
				if (((CheckBox)row.FindControl("confirm")).Checked == false) {
					continue;
				}
				DataRow drow = (DataRow)row.DataItem;
				CourseGradeViewModel grade = new CourseGradeViewModel();
				grade.Course_Id = Convert.ToInt32(drow["CourseID"]);
				grade.CourseName=(String)drow["CourseName"];
				grade.Grade=Convert.ToInt32(((TextBox)row.FindControl("grade")).Text);
				grade.StudentId = Convert.ToInt32(drow["StudentID"]);
				grade.StudentName = (String)drow["StudentName"];
				grade.Type = (CourseType)Enum.Parse(typeof(CourseType),(String)drow["CourseType"]);
				ctrl.UpdateGrade(grade);
			}
        }

        protected void ButtonSignout_Click(object sender, EventArgs e)
        {
            Session.Remove("New");
            Session.RemoveAll();
            Response.Redirect("Login.aspx");
        }
    }
}