
$(document).ready(function () {


	parallax.add($("#courses"))
			.add($("#index"));

	parallax.background = $("body");

	//Clears each page navigation on load
	parallax.preload = function(){
		rightKey = leftKey = topKey = bottomKey = "";
		$(".control").hide();
	};


	//Setting up page navigation
	parallax.index.onload=function(){
		setCourses("courses", "Courses");
	};
	
	function setCourses(page, text){
		$("#CoursesControl").show().unbind('click').click(function(){
			parallax[page].left();
		});
	}
	
	
	$(".control").hide();
	parallax.index.show();

});
