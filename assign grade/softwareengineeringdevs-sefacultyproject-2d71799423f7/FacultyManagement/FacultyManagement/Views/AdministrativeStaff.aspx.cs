﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;
using FacultyManagement.Models.YearOfStudy;
using System.Windows.Forms;

namespace FacultyManagement.Views
{
    public partial class AdministrativeStaff : System.Web.UI.Page
    {
        private ViewHomeController homeCtrl = new ViewHomeController();
        private ViewAdministrativeStaffController adminCtrl = new ViewAdministrativeStaffController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["New"]!= null)
            {
                string Type = (string)Session["New"];
                Type = Type.Split('.')[0];
                if (Type == "AdministrativeStaff")
                {
                    ;
                }
                else
                {
                    Response.Redirect(Type + ".aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            StudentViewModel var = new StudentViewModel();

            var.Name = StudentAddName.Text;
            var.Group = new StudentsGroup();
            var.Group.Name = StudentAddGroup.Text;
            var.Username = StudentAddUsername.Text;
            var.Password = StudentAddPassword.Text;

            if (StudentAddPassword.Text == StudentAddPassword.Text && StudentAddPassword.Text!=string.Empty)
            {
                if(adminCtrl.Create(var))
                {
                    LabelAddInteract.Text = "Studentul a fost adaugat";
                }
                else
                {
                    LabelAddInteract.Text = "Eroare, daca persista contactati administratorul";
                }
            }
            else
            {
                LabelAddInteract.Text = "Parolele nu corespund";
            }

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            StudentViewModel var = new StudentViewModel();

            string username = StudentEditUsername.Text;

            var.Name = StudentEditNewName.Text;
            var.Group = new StudentsGroup();
            var.Group.Name = StudentEditNewGroup.Text;
            var.Username = StudentEditNewUsername.Text;

                if(adminCtrl.Edit(username,var))
                {
                    LabelEditInteract.Text = "Studentul a fost modificat";
                }
                else
                {
                    LabelEditInteract.Text = "Eroare, verifica daca datele sunt valide - daca persista contactati administratorul";
                }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (StudentDeleteUsername.Text == StudentDeleteConfirmUsername.Text)
            {
                if (adminCtrl.Delete(StudentDeleteUsername.Text))
                {
                    LabelDeleteInteract.Text = "Userul cu idul " + StudentDeleteUsername.Text + " a fost sters";
                }
                else
                {
                    LabelDeleteInteract.Text = "Userul cu idul " + StudentDeleteUsername.Text + " nu a fost sters, probabil ca nu exista";
                }
            }
            else
            {
                LabelDeleteInteract.Text = "Idurile introduse nu corespund";
            }
        }

        protected void ButtonDetails_Click(object sender, EventArgs e)
        {
            StudentViewModel var = new StudentViewModel();

            var = adminCtrl.Details(StudentDetailsUsername.Text);

            if(var!=null)
            {
                StudentDetailsReadUser.Text = var.Username;
                StudentDetailsReadName.Text = var.Name;
                StudentDetailsReadGroupName.Text = var.Group.Name;
                
            }
        }

        protected void ButtonSignout_Click(object sender, EventArgs e)
        {
            Session.Remove("New");
            Session.RemoveAll();
            Response.Redirect("Login.aspx");
        }
    }
}