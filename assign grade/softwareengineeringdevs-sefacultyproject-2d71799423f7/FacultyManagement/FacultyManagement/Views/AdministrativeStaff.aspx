﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdministrativeStaff.aspx.cs" Inherits="FacultyManagement.Views.AdministrativeStaff" %>


<!DOCTYPE html>
<html>

<head>
	<script type="text/javascript" src="resources/jq.js"></script>
	<script type="text/javascript" src="resources/parallax.min.js"></script>
	<script type="text/javascript" src="resources/AdministrativeStaff.js"></script>
	<link rel="Stylesheet" type="text/css" href="resources/C3.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>Administrative Staff</title>
	
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-34546066-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	
</head>


<body class="bc">
    
    

    <form runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server">
     </asp:ScriptManager>

    <div class="fixed-size-square">
    </div>
    
    <header>
        <div class="topbar"></div>
        <a href="" class="toggle">≡</a>
        <div class="menu">
            <ul>
                <a id="CreateControl"><li>Create</li></a>
                <a id="DeleteControl"><li>Delete</li></a>
                <a id="DetailsControl" ><li>Details</li></a>
                <a id="EditControl" ><li>Edit</li></a>
            </ul>
        </div>
        <asp:Button ID="ButtonSignout" type = "button" class="singout" runat="server" Text="SIGN OUT" OnClick="ButtonSignout_Click" />
    </header>
    
	
    <div id="index">
        <div class="Edit">
            <hr class="first_line">
            <asp:Label ID="LabelHomePage" runat="server" class="edit_lable" Text="Home Page ADMIN"></asp:Label>
        </div>
    </div>
	

    <div id="add_user">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <hr class="first_line">
                    <label class="edit_lable">ADD STUDENT</label>
                    <div class="Edit">
            
                        <asp:TextBox ID="StudentAddName" runat="server" type="text" placeholder="Student Name" name="name"></asp:TextBox>
                        <asp:TextBox ID="StudentAddGroup" type="text" placeholder="Student Group" name="group" runat="server"></asp:TextBox>
                        <asp:TextBox ID="StudentAddUsername" type="text" placeholder="Username" name="user" runat="server"></asp:TextBox>
                        <asp:TextBox ID="StudentAddPassword" type="password" placeholder="Password" name="password" runat="server"></asp:TextBox>
                        <asp:TextBox ID="StudentAddConfirmPassword" type="password" placeholder="Confirm Password" name="confirm" runat="server"></asp:TextBox>
                        <input id="ButtonAdd" runat="server" type="button" value="ADD" onserverclick="ButtonAdd_Click" />
                 
                    </div>
        
                    <div class="infobox">
                        <asp:Label ID="LabelAddInteract" runat="server" Text=""></asp:Label>
                    </div>   
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="edit_user">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <hr class="first_line">
                    <label class="edit_lable">EDIT STUDENT</label>
                    <div class="Edit">
            
                        <asp:TextBox ID="StudentEditUsername" runat="server" type="text" placeholder="Student Username" name="name"></asp:TextBox>
                        <asp:TextBox ID="StudentEditNewName" runat="server" type="text" placeholder="New Student Name" name="name"></asp:TextBox>
                        <asp:TextBox ID="StudentEditNewGroup" runat="server" type="text" placeholder="New Student Group" name="group"></asp:TextBox>
                        <asp:TextBox ID="StudentEditNewUsername" runat="server" type="text" placeholder="New Username" name="user"></asp:TextBox>
                        <input id="ButtonEdit" runat="server" type="button" value="EDIT" onserverclick="ButtonEdit_Click" />
                
                    </div>
                        
                    <div class="infobox">
                        <asp:Label ID="LabelEditInteract" runat="server" Text=""></asp:Label>
                    </div>  
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="delete_user">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <hr class="first_line">
                    <label class="edit_lable">DELETE STUDENT</label>
                    <div class="Edit">
		
                        <asp:TextBox ID="StudentDeleteUsername" runat="server" type="text" placeholder="Student Id" name="user"></asp:TextBox>
                        <asp:TextBox ID="StudentDeleteConfirmUsername" runat="server" type="text" placeholder="Confirm Student Id" name="user"></asp:TextBox>
                        <input id="ButtonDelete" runat="server" type="button" value="DELETE" onserverclick="ButtonDelete_Click" />

                    </div>
                    <div class="infobox">
                        <asp:Label ID="LabelDeleteInteract" runat="server" Text=""></asp:Label>
                    </div>    
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>
		
    <div id="user_details">

        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>

                    <hr class="first_line">
                    <label class="edit_lable">STUDENT DETAILS</label>
                    <div class="Edit">
            
		                    <asp:TextBox ID="StudentDetailsUsername" runat="server" type="text" placeholder="Username" name="user"></asp:TextBox>
                            <input id="ButtonDetails" runat="server" type="button" value="DETAILS" onserverclick="ButtonDetails_Click" />
                
			
                    </div>
        
                    <div class="infobox">
                        <asp:TextBox ID="StudentDetailsReadUser" runat="server" type="text" placeholder="Username" name="user"></asp:TextBox>
                        <br>
                        <asp:TextBox ID="StudentDetailsReadName" runat="server" type="text" placeholder="Name" name="user"></asp:TextBox>
                        <br>
                        <asp:TextBox ID="StudentDetailsReadGroupName" runat="server" type="text" placeholder="Group" name="user"></asp:TextBox>
                        </div>

                </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
    <script>
        (function(){
            var body = $('body');
            $('.toggle').bind('click', function(){
                body.toggleClass('menu-open');
                return false;
            });
        })();
    </script>
       </form>
</body>

</html>



