﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecoverPassword.aspx.cs" Inherits="FacultyManagement.Views.RecoverPassword" %>

<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="resources/C1.css">
  <title>Recover</title>
</head>


<body>

    <img src="resources/Logo.png" id="logo">
    
	<form id="form" runat="server">
		<label id="Recover_Label" type="Singin">Recover</label>
		<br>
		<label id="Username_Labe" type="Username">User</label>
		<br>
        <asp:TextBox ID="Username_TextBox" runat="server" type="Username" name="User" Font-Names="Comic Sans MS" Font-Size="Medium"></asp:TextBox>
		<br>
		<label id="Email_Label" type="Password">Email</label>
		<br>
        <asp:TextBox ID="Email_TextBox" runat="server" type="Username" name="Email" Font-Names="Comic Sans MS" Font-Size="Medium"></asp:TextBox>
		<br>
        <br>
        <asp:DropDownList ID="Type_Select" runat="server" BackColor="#E4D5C3" Width="300px" Font-Names="Comic Sans MS">
            <asp:ListItem>Student</asp:ListItem>
            <asp:ListItem>Teacher</asp:ListItem>
            <asp:ListItem>AdministrativeStaff</asp:ListItem>
            <asp:ListItem>ChiefOfDepartment</asp:ListItem>
        </asp:DropDownList>
        <br>
        <br>
        <asp:Button ID="RecoverButton" runat="server" class="submit-button" type="submit" Text="Submit" OnClick="RecoverButton_Click" Font-Names="Comic Sans MS" />
        <br>
        <br>
        <a href="ChangePassword.aspx">If you have reset code click here</a>
	</form>

</body>

</html>
