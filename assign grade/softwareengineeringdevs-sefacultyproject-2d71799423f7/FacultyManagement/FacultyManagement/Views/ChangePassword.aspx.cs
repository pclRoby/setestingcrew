﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Views
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        private ViewHomeController ctrl = new ViewHomeController();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ChangeButton_Click(object sender, EventArgs e)
        {
            ChangePasswordViewModel var = new ChangePasswordViewModel();

            var.ResetCode = TextBoxResetCode.Text;
            var.NewPassword = TextBoxPassword.Text;
            UserType type;
            Enum.TryParse(Type_Select.SelectedItem.Text, out type);
            var.TypeOfUser = type;
            var.Username = TextBoxUsername.Text;


            if (TextBoxPassword.Text == TextBoxConfirmPassword.Text)
            {
                if (ctrl.ChangePassword(var))
                {
                    TextBoxResetCode.Text = "Succes";
                }
                else
                {
                    TextBoxResetCode.Text = "Failed";
                }
            }
        }
    }
}