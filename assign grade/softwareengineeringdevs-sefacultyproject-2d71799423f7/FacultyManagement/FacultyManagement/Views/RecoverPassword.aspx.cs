﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Views
{
    public partial class RecoverPassword : System.Web.UI.Page
    {
        private ViewHomeController ctrl = new ViewHomeController();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RecoverButton_Click(object sender, EventArgs e)
        {
            RecoverPasswordViewModel var = new RecoverPasswordViewModel();

            string user = Username_TextBox.Text;
            string email = Email_TextBox.Text;
            UserType type;
            Enum.TryParse(Type_Select.SelectedItem.Text, out type);

            var.Username = user;
            var.Email = email;
            var.TypeOfUser = type;

            if (ctrl.RecoverPassword(var) == true)
            {
                Username_TextBox.Text = "  New password was sent";
            }
            else
            {
                Username_TextBox.Text = "  Wrong data";
            }
        }
    }
}