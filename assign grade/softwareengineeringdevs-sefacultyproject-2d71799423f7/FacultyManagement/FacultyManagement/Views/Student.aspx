﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Student.aspx.cs" Inherits="FacultyManagement.Views.Student" %>

<!DOCTYPE html>

<html>

<head>
	<script type="text/javascript" src="resources/jq.js"></script>
	<script type="text/javascript" src="resources/parallax.min.js"></script>
	<script type="text/javascript" src="resources/Student.js"></script>
	<link rel="Stylesheet" type="text/css" href="resources/C3.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>Student</title>
	
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-34546066-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	
</head>


<body class="bc">
    
    <form runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server">
     </asp:ScriptManager>

    <div class="fixed-size-square">
    </div>
    
    <header>
        <div class="topbar"></div>
        <a href="" class="toggle">≡</a>
        <div class="menu">
            <ul>
                <a id="CoursesControl"><li>Courses</li></a>
            </ul>
        </div>
        <asp:Button ID="ButtonSignout" type = "button" class="singout" runat="server" Text="SIGN OUT" OnClick="ButtonSignout_Click" />
    </header>
    
	
    <div id="index">
        <div class="Edit">
            <hr class="first_line">
            <asp:Label ID="LabelHomePage" runat="server" class="edit_lable" Text="Home Page Student"></asp:Label>
        </div>
    </div>
	

    <div id="courses">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <hr class="first_line">
                    <label class="edit_lable">VIEW COURSES</label>
                    <div class="Edit">
                        <asp:Label ID="Label1" runat="server" class="edit_lable" Text="Aici o sa vezi cursurile"></asp:Label>
                        <input id="ButtonCourses" runat="server" type="button" value="COURSES" onserverclick="ButtonCourses_Click" />

                    </div>
        
                    <div class="infobox">

                    </div>
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
    <script>
        (function(){
            var body = $('body');
            $('.toggle').bind('click', function(){
                body.toggleClass('menu-open');
                return false;
            });
        })();
    </script>
       </form>
</body>

</html>