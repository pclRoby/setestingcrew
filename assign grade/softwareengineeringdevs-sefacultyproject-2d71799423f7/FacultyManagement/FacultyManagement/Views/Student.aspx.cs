﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Views
{
    public partial class Student : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			if (Session["New"] != null) {
				string Type = (string)Session["New"];
				Type = Type.Split('.')[0];

				if (Type == "Student") {
					//ce vreau sa fac
				}
				else {
					Response.Redirect(Type + ".aspx");
				}
			}
			else {
				Response.Redirect("Login.aspx");
			}
        }

        protected void ButtonCourses_Click(object sender, EventArgs e)
        {
            //aici iau toate cursurile utilizatorului si le afisez
            //in username e userul logat, deci o sa returnez toate cursurile la care e inscris userul username
            string username = (string)Session["New"];
            username = username.Split('.')[1];
        }

        protected void ButtonSignout_Click(object sender, EventArgs e)
        {
            Session.Remove("New");
            Session.RemoveAll();
            Response.Redirect("Login.aspx");
        }
    }
}