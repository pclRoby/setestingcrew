﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.Models.StudentsGrade
{
    interface IStudentsGrade
    {
        [Key]
        int Id { set; get; }

        [Required]
        int StudentId { set; get; }

        [Required]
        int Grade { set; get; }
    }
}
