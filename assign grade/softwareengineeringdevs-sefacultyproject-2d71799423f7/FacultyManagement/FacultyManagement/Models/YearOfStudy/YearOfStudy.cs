﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.Faculty;

namespace FacultyManagement.Models.YearOfStudy
{
	public class YearOfStudy
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		public virtual ICollection<StudentsGroup> StudentsGroups { get; set; }

		public virtual ICollection<Semester> Semesters { get; set; }

		public virtual ICollection<StudyLanguage> StudyLanguages { get; set; }
	}
}