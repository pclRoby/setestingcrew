﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.Users;

namespace FacultyManagement.Models.YearOfStudy
{
	public class StudentsGroup
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		public virtual ICollection<Student> Students { get; set; }

		public virtual YearOfStudy YearOfStudy { get; set; }
	}
}