﻿using FacultyManagement.Models.Faculty;
using FacultyManagement.Models.Courses;
using System.Collections.Generic;

namespace FacultyManagement.Models.Users
{
	public class Teacher : IUser
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Username { get; set; }

		public string Password { get; set; }

		public virtual Department Department { get; set; }

        public List<ICourse> Courses { get; set; }
	}
}