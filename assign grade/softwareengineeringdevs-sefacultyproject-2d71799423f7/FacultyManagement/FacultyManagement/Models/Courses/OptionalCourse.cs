﻿using FacultyManagement.Models.YearOfStudy;

namespace FacultyManagement.Models.Courses
{
	public class OptionalCourse : ICourse
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public int Credits { get; set; }

		public virtual Semester Semester { get; set; }

        public int Teacher_Id { get; set; }
	}
}