﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FacultyManagement.Models.Users;
using FacultyManagement.Repositories;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Services
{
	public class TeacherService
	{
		private readonly IUserRepository<Teacher> teacherRepository;

		public TeacherService(IUserRepository<Teacher> teacherRepository)
		{
			this.teacherRepository = teacherRepository;
		}

		public List<TeacherViewModel> GetAll()
		{
			var teacherEntities = this.teacherRepository.GetAll();
			return teacherEntities.Select(Mapper.Map<Teacher, TeacherViewModel>).ToList();
		}

		public TeacherViewModel FindById(int id)
		{
			var teacherEntity = this.teacherRepository.FindById(id);
			return Mapper.Map<Teacher, TeacherViewModel>(teacherEntity);
		}

		public void Add(TeacherViewModel teacherViewModel)
		{
			var teacherEntity = Mapper.Map<TeacherViewModel, Teacher>(teacherViewModel);
			teacherEntity.Password = LoginService.EncodePassword(teacherEntity.Password);
			this.teacherRepository.Add(teacherEntity);
		}

		public void DeleteUser(int id)
		{
			this.teacherRepository.Delete(id);
		}

		public void UpdateUser(TeacherViewModel teacherViewModel)
		{
			var teacherEntity = Mapper.Map<TeacherViewModel, Teacher>(teacherViewModel);
			this.teacherRepository.Update(teacherEntity);
		}
	}
}