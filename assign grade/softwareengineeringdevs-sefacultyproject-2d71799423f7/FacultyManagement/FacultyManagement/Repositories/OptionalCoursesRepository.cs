﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using FacultyManagement.Models;
using FacultyManagement.Models.Courses;

namespace FacultyManagement.Repositories
{
	public class OptionalCoursesRepository : ICourseRepository<OptionalCourse>
	{
		public void Add(OptionalCourse course)
		{
			using (var context = new FacultyManagementContext())
			{
				context.OptionalCourses.AddOrUpdate(course);
				context.SaveChanges();
			}
		}

		public void Delete(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				var courseToDelete = this.FindById(id);
				if (courseToDelete == null)
				{
					throw new KeyNotFoundException("Optional Course not found!");
				}
				context.OptionalCourses.Remove(courseToDelete);
				context.SaveChanges();
			}
		}

		public void Update(OptionalCourse newCourse)
		{
			using (var context = new FacultyManagementContext())
			{
				context.OptionalCourses.AddOrUpdate(newCourse);
				context.SaveChanges();
			}
		}

		public OptionalCourse FindById(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				return context.OptionalCourses.Find(id);
			}
		}

		public List<OptionalCourse> GetAll()
		{
			using (var context = new FacultyManagementContext())
			{
				return context.OptionalCourses.ToList();
			}
		}
	}
}