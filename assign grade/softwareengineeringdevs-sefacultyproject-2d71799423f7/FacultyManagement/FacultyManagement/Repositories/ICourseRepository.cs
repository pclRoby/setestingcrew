﻿using System.Collections.Generic;

namespace FacultyManagement.Repositories
{
	public interface ICourseRepository<Course>
	{
		void Add(Course course);

		void Delete(int id);

		void Update(Course newCourse);

		Course FindById(int id);

		List<Course> GetAll();
	}
}
