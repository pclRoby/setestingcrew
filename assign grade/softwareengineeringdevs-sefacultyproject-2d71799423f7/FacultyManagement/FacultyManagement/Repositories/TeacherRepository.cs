﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using FacultyManagement.Models;
using FacultyManagement.Models.Users;

namespace FacultyManagement.Repositories
{
	public class TeacherRepository : IUserRepository<Teacher>
	{
		public void Update(Teacher newUser)
		{
			using (var context = new FacultyManagementContext())
			{
				var teacherDepartment = context.Departments.FirstOrDefault(department => department.Name.Equals(newUser.Department.Name));
				if (teacherDepartment == null)
				{
					throw new DepartmentNotFoundException("Department " + newUser.Department + " not found");
				}

				var oldTeacher = this.FindById(newUser.Id);
				this.Delete(newUser.Id);
				this.Add(new Teacher
				{
					Name = newUser.Name,
					Username = newUser.Username,
					Password = oldTeacher.Password,
					Department = teacherDepartment
				});
			}
		}

		public void Add(Teacher user)
		{
			using (var context = new FacultyManagementContext())
			{
				var teacherDepartment = context.Departments.FirstOrDefault(department => department.Name.Equals(user.Department.Name));
				if (teacherDepartment == null)
				{
					throw new DepartmentNotFoundException("Department " + user.Department + " not found");
				}
				user.Department = teacherDepartment;
				context.Teachers.AddOrUpdate(user);
				context.SaveChanges();
			}
		}

		public void Delete(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				var userToDelete = this.FindById(id);
				if (userToDelete == null)
				{
					throw new KeyNotFoundException("User not found!");
				}
				context.Entry(userToDelete).State = EntityState.Deleted;
				context.SaveChanges();
			}
		}

		public Teacher FindById(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				return context.Teachers.Include(x => x.Department).ToList().FirstOrDefault(teacher => teacher.Id == id);
			}
		}

		public List<Teacher> GetAll()
		{
			using (var context = new FacultyManagementContext())
			{
				return context.Teachers.Include(x => x.Department).Where(x => x.Department != null).ToList();
			}
		}
	}

	public class DepartmentNotFoundException : Exception
	{
		public DepartmentNotFoundException(string s) : base(s)
		{
		}
	}
}