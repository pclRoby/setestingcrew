﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using FacultyManagement.Models;
using FacultyManagement.Models.Users;

namespace FacultyManagement.Repositories
{
	public class StudentRepository : IUserRepository<Student>
	{
		public void Update(Student newUser)
		{
			using (var context = new FacultyManagementContext())
			{
				var studentGroup = context.StudentsGroups.FirstOrDefault(group => group.Name.Equals(newUser.Group.Name));
				if (studentGroup == null)
				{
					throw new StudentGroupNotFoundException("Group " + newUser.Group + " not found");
				}

				var oldStudent = this.FindById(newUser.Id);
				this.Delete(newUser.Id);
				this.Add(new Student
				{
					Name = newUser.Name, 
					Username = newUser.Username, 
					Password = oldStudent.Password == null ? oldStudent.Password : newUser.Password, 
					Group = studentGroup
				});
			}
		}

		public void Add(Student user)
		{
			using (var context = new FacultyManagementContext())
			{
				var studentGroup = context.StudentsGroups.First(group => group.Name.Equals(user.Group.Name));
				user.Group = studentGroup;
				context.Students.AddOrUpdate(user);
				context.SaveChanges();
			}
		}

		public void Delete(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				var userToDelete = this.FindById(id);
				if (userToDelete == null)
				{
					throw new KeyNotFoundException("User not found!");
				}
				context.Entry(userToDelete).State = EntityState.Deleted;
				context.SaveChanges();
			}
		}

		public Student FindById(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				return context.Students.Include(x => x.Group).ToList().FirstOrDefault(student => student.Id == id);
			}
		}

		public List<Student> GetAll()
		{
			using (var context = new FacultyManagementContext())
			{
				return context.Students.Include(x => x.Group).ToList();
			}
		}
	}

	public class StudentGroupNotFoundException : Exception
	{
		public StudentGroupNotFoundException(string s) : base(s)
		{
		}
	}
}