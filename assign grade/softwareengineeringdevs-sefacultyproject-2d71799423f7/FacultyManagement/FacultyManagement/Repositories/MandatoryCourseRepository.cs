﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using FacultyManagement.Models;
using FacultyManagement.Models.Courses;

namespace FacultyManagement.Repositories
{
	public class MandatoryCourseRepository : ICourseRepository<MandatoryCourse>
	{
		public void Add(MandatoryCourse course)
		{
			using (var context = new FacultyManagementContext())
			{
				context.MandatoryCourses.AddOrUpdate(course);
				context.SaveChanges();
			}
		}

		public void Delete(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				var courseToDelete = this.FindById(id);
				if (courseToDelete == null)
				{
					throw new KeyNotFoundException("Mandatory Course not found!");
				}
				context.MandatoryCourses.Remove(courseToDelete);
				context.SaveChanges();
			}
		}

		public void Update(MandatoryCourse newCourse)
		{
			using (var context = new FacultyManagementContext())
			{
				context.MandatoryCourses.AddOrUpdate(newCourse);
				context.SaveChanges();
			}
		}

		public MandatoryCourse FindById(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				return context.MandatoryCourses.Find(id);
			}
		}

		public List<MandatoryCourse> GetAll()
		{
			using (var context = new FacultyManagementContext())
			{
				return context.MandatoryCourses.ToList();
			}
		}
	}
}