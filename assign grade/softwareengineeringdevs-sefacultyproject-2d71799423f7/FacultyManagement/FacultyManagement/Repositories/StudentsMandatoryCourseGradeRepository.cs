﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.StudentsGrade;
using FacultyManagement.Models;
using FacultyManagement.Models.Users;
using System.Data.Entity.Migrations;

namespace FacultyManagement.Repositories
{
    public class StudentsMandatoryCourseGradeRepository : IStudentsGradeRepository<StudentsMandatoryCourseGrade>
    {
        public void Add(StudentsMandatoryCourseGrade studentGrade)
        {
            using (var context = new FacultyManagementContext())
            {
                context.StudentsMandatoryCourseGrade.AddOrUpdate(studentGrade);
                context.SaveChanges();

            }
        }

		public StudentsMandatoryCourseGrade FindWithoutID(int studID, int courseID) {
			using (var context = new FacultyManagementContext()) {
				return context.StudentsMandatoryCourseGrade.Where(s => s.MandatoryCourse_Id == 2 && s.StudentId == studID).FirstOrDefault();
			}
		}

        public void Delete(int id)
        {
            using (var context = new FacultyManagementContext())
            {
                var userGrade = this.FindById(id);
                if (userGrade == null)
                {
                    throw new KeyNotFoundException("User not found!");
                }
                context.StudentsMandatoryCourseGrade.Remove(userGrade);
                context.SaveChanges();
            }
        }


        public void Update(StudentsMandatoryCourseGrade userGrade)
        {
            using (var context = new FacultyManagementContext())
            {
                context.StudentsMandatoryCourseGrade.AddOrUpdate(userGrade);
                context.SaveChanges();
            }
        }


        public StudentsMandatoryCourseGrade FindById(int id)
        {
            using (var context = new FacultyManagementContext())
            {
                return context.StudentsMandatoryCourseGrade.Find(id);
            }
        }

        public List<ICourse> FindStudentCourse(int studentId)
        {
            List<ICourse> list = new List<ICourse>();
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsMandatoryCourseGrade studentGrade in context.StudentsMandatoryCourseGrade.ToList())
                {
                    if (studentGrade.StudentId == studentId)
                        list.Add(context.MandatoryCourses.Find(studentGrade.MandatoryCourse_Id));
                }
            }
            return list;
        }

        public List<Student> FindCourseStudents(int courseId)
        {
            List<Student> list = new List<Student>();
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsMandatoryCourseGrade studentGrade in context.StudentsMandatoryCourseGrade.ToList())
                {
                    if (studentGrade.MandatoryCourse_Id == courseId)
                        list.Add(context.Students.Find(studentGrade.StudentId));
                }
            }
            return list;
        }

        public int GetStudentGrade(int studentId, int courseId)
        {
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsMandatoryCourseGrade studentGrade in context.StudentsMandatoryCourseGrade.ToList())
                    if (studentGrade.StudentId == studentId && studentGrade.MandatoryCourse_Id == courseId)
                        return studentGrade.Grade;
                return -1;
            }
        }
    }
}