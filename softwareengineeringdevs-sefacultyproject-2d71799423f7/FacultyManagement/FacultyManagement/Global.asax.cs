﻿using System.Data.Entity;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac.Integration.Mvc;
using FacultyManagement.App_Start;
using FacultyManagement.Bootstrap;
using FacultyManagement.Models;

namespace FacultyManagement
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : HttpApplication
	{
		protected void Application_Start()
		{
			var container = BootstrapRegistrar.BuildContainer();
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
			
			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AuthConfig.RegisterAuth();
			AutoMapperConfig.CreateMaps();
            //Database.SetInitializer<Models.FacultyManagementContext>(null);  
		}
	}
}