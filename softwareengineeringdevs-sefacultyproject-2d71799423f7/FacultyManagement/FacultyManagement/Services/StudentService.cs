﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FacultyManagement.Models.Users;
using FacultyManagement.Repositories;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Services
{
	public class StudentService
	{
		private readonly IUserRepository<Student> studentRepository;

		public StudentService(IUserRepository<Student> studentRepository)
		{
			this.studentRepository = studentRepository;
		}

		public List<StudentViewModel> GetAll()
		{
			var studentEntities = this.studentRepository.GetAll();
			return studentEntities.Select(Mapper.Map<Student, StudentViewModel>).ToList();
		}

		public StudentViewModel FindById(int id)
		{
			var studentEntity = this.studentRepository.FindById(id);
			return Mapper.Map<Student, StudentViewModel>(studentEntity);
		}

		public void Add(StudentViewModel studentViewModel)
		{
			var studentEntity = Mapper.Map<StudentViewModel, Student>(studentViewModel);
			studentEntity.Password = LoginService.EncodePassword(studentEntity.Password);
			this.studentRepository.Add(studentEntity);//deci tu chemi serviciul? da, cel putin asa ai facut tu in controllerul tau
            //si eu m-am luat dupa tine, dar verifica daca sunt grupe in table ca poate doar aia e chestia ok
		}

		public void Delete(int id)
		{
			this.studentRepository.Delete(id);
		}

		public void Update(StudentViewModel studentViewModel)
		{

			var studentEntity = Mapper.Map<StudentViewModel, Student>(studentViewModel);
			this.studentRepository.Update(studentEntity);
		}
	}
}