﻿using System;
using System.Linq;
using System.Web.Security;
using FacultyManagement.Models.Users;
using FacultyManagement.Repositories;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Services
{
	public class LoginService
	{
		private readonly IUserRepository<AdminStaff> adminStaffRepository;
		private readonly IUserRepository<Teacher> teacherRepository;
		private readonly IUserRepository<ChiefOfDepartment> codRepository;
		private readonly IUserRepository<Student> studentRepository;

		public LoginService(IUserRepository<AdminStaff> adminStaffRepository, IUserRepository<Teacher> teacherRepository, IUserRepository<ChiefOfDepartment> codRepository, IUserRepository<Student> studentRepository)
		{
			this.adminStaffRepository = adminStaffRepository;
			this.teacherRepository = teacherRepository;
			this.codRepository = codRepository;
			this.studentRepository = studentRepository;
		}

		public bool ValidateUser(ref LoginViewModel user)
		{
			var passwordToEncrypt = EncodePassword(user.Password);
			var repositoryUser = this.GetRepositoryUser(user.Username, user.TypeOfUser);
			if (repositoryUser == null)
			{
				throw new UserNotFoundException(user.Username);
			}
			return repositoryUser.Password.Equals(passwordToEncrypt);
		}

		public IUser GetRepositoryUser(string username, UserType type)
		{
			switch (type)
			{
				case UserType.AdministrativeStaff:
				{
					return this.adminStaffRepository.GetAll().FirstOrDefault(u => u.Username.Equals(username));
				}
				case UserType.ChiefOfDepartment:
				{
					return this.codRepository.GetAll().FirstOrDefault(u => u.Username.Equals(username));
				}
				case UserType.Student:
				{
					return this.studentRepository.GetAll().FirstOrDefault(u => u.Username.Equals(username));
				}
				case UserType.Teacher:
				{
					return this.teacherRepository.GetAll().FirstOrDefault(u => u.Username.Equals(username));
				}
			}
			return null;
		}

		public void RecoverPassword(RecoverPasswordViewModel recoverViewModel)
		{
			var user = GetRepositoryUser(recoverViewModel.Username,recoverViewModel.TypeOfUser);
			if (user == null)
			{
				throw new UserNotFoundException(recoverViewModel.Username);
			}
			var resetCode = SendResetEmail(recoverViewModel);
			this.ResetPassword(user, resetCode);
		}

		private static string SendResetEmail(RecoverPasswordViewModel recoverViewModel)
		{
			var resetCode = GenerateResetCode();
			EmailService.SendEmail(recoverViewModel.Email, recoverViewModel.Username, resetCode);
			return resetCode;
		}

		private static string GenerateResetCode(int passwordLength = 10, int noOfNonAlphaNumChar = 0)
		{
			return Membership.GeneratePassword(passwordLength, noOfNonAlphaNumChar);
		}

		public void ResetPassword(IUser user, string resetCode)
		{
			user.Password = EncodePassword(resetCode);
			if (user is AdminStaff)
			{
				this.adminStaffRepository.Update((AdminStaff) user);
			}
			else if (user is ChiefOfDepartment)
			{
				this.codRepository.Update((ChiefOfDepartment) user);
			}
			else if (user is Student)
			{
				this.studentRepository.Update((Student) user);
			}
			else if (user is Teacher)
			{
				this.teacherRepository.Update((Teacher) user);
			}
		}

		public bool IsResetCodeValid(IUser user, string resetCode)
		{
			return user.Password.Equals(EncodePassword(resetCode));
		}

		public static string EncodePassword(string value)
		{
			var hash = System.Security.Cryptography.SHA1.Create();
			var encoder = new System.Text.ASCIIEncoding();
			var combined = encoder.GetBytes(value ?? "");
			return BitConverter.ToString(hash.ComputeHash(combined)).ToLower().Replace("-", "");
		}
	}

	public class UserNotFoundException : Exception
	{
		public UserNotFoundException(string username)
			: base("User " + username + " not found!")
		{
		}
	}
}