﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.ViewModel;
using FacultyManagement.Services;
using FacultyManagement.Repositories;

namespace FacultyManagement.Controllers
{
    public class ViewAdministrativeStaffController : Controller
    {
        private readonly StudentService studentService = new StudentService(new StudentRepository());

        public StudentViewModel Details(string  username)
        {
            int id = this.studentService.GetAll().First(student => student.Username.Equals(username)).Id;
            var studentViewModel = this.studentService.FindById(id);
            return studentViewModel;
        }

        public bool Create(StudentViewModel var)
        {
            if (ModelState.IsValid)
            {
                this.studentService.Add(var);
                return true;
            }
            return false;
        }

        public bool Edit(string username, StudentViewModel var)
        {
            int id = this.studentService.GetAll().First(student => student.Username.Equals(username)).Id;
            var studentViewModel = this.studentService.FindById(id);
            if (studentViewModel == null)
            {
                return false;
            }
            else
            {
                if(ModelState.IsValid)
                {
                    try
                    {

                        var.Id = studentViewModel.Id;
                        var.Password = studentViewModel.Password;

                        this.studentService.Update(var);
                        return true;
                    }
                    catch (StudentGroupNotFoundException exc)
                    {
                        ModelState.AddModelError("groupNotFound", exc.Message);
                        return false;
                    }
                }
            }
            return true;
        }

        public bool Delete(string username)
        {
            int id = this.studentService.GetAll().First(student => student.Username.Equals(username)).Id;
            var studentViewModel = this.studentService.FindById(id);
            if (studentViewModel == null)
            {
                return false;
            }
            else
            {
                this.studentService.Delete(id);
                return true;
            }
        }

    }
}