﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Repositories;
using FacultyManagement.Models.Courses;
using FacultyManagement.Services;
using FacultyManagement.Models;
using FacultyManagement.Models.StudentsGrade;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Controllers
{
    public class ViewTeacherController : Controller
    {
        private readonly TeacherService teacherService = new TeacherService(new TeacherRepository());
		private readonly StudentsFacultativeCourseGradeRepository facultativeGrades = new StudentsFacultativeCourseGradeRepository();
		private readonly StudentsMandatoryCourseGradeRepository mandatoryGrades = new StudentsMandatoryCourseGradeRepository();
		private readonly StudentsOptionalCourseGradeRepository optionalGrades = new StudentsOptionalCourseGradeRepository();

        public List<ICourse> Courses(string username)
        {
            int id = this.teacherService.GetAll().First(teacher => teacher.Username.Equals(username)).Id;
            return this.teacherService.FindById(id).Courses;
        }

		public void UpdateGrade(CourseGradeViewModel grade) {

			switch (grade.Type) {
				case CourseType.Mandatory:
					StudentsMandatoryCourseGrade gr1= mandatoryGrades.FindWithoutID(grade.StudentId,grade.Course_Id);
					gr1.Grade = grade.Grade;
					mandatoryGrades.Update(gr1);
					break;
				case CourseType.Facultative:
					StudentsFacultativeCourseGrade gr2= facultativeGrades.FindWithoutID(grade.StudentId,grade.Course_Id);
					gr2.Grade = grade.Grade;
					facultativeGrades.Update(gr2);
					break;
				case CourseType.Optional:
					StudentsOptionalCourseGrade gr3= optionalGrades.FindWithoutID(grade.StudentId,grade.Course_Id);
					gr3.Grade = grade.Grade;
					optionalGrades.Update(gr3);
					break;
				default:
					break;
			}
		}

		public List<CourseGradeViewModel> GetGradesByTeacher(String teachUserName) {
			List<CourseGradeViewModel> list=new List<CourseGradeViewModel>();
			using(FacultyManagementContext context=new FacultyManagementContext()){
				var objList = from Grades in context.StudentsFacultativeCourseGrade
							join courses in context.FacultativeCourses on Grades.FacultativeCourse_Id equals courses.Id
							join studs in context.Students on Grades.StudentId equals studs.Id
							select new {
								Grades,
								CourseName = courses.Name,
								StudentName = studs.Name
							};
				foreach(var tmp in objList){
					CourseGradeViewModel cg=new CourseGradeViewModel();
					cg.Course_Id = tmp.Grades.FacultativeCourse_Id;
					cg.Grade = tmp.Grades.Grade;
					cg.Id = tmp.Grades.Id;
					cg.StudentId = tmp.Grades.StudentId;
					cg.Type = CourseType.Facultative;
					cg.CourseName = tmp.CourseName;
					cg.StudentName = tmp.StudentName;
					list.Add(cg);
				}
				var objList1 = from Grades in context.StudentsOptionalCourseGrade
							  join courses in context.OptionalCourses on Grades.OptionalCourse_Id equals courses.Id
							  join studs in context.Students on Grades.StudentId equals studs.Id
							  select new {
								  Grades,
								  CourseName = courses.Name,
								  StudentName = studs.Name
							  };
				foreach (var tmp in objList1) {
					CourseGradeViewModel cg = new CourseGradeViewModel();
					cg.Course_Id = tmp.Grades.OptionalCourse_Id;
					cg.Grade = tmp.Grades.Grade;
					cg.Id = tmp.Grades.Id;
					cg.StudentId = tmp.Grades.StudentId;
					cg.Type = CourseType.Optional;
					cg.CourseName = tmp.CourseName;
					cg.StudentName = tmp.StudentName;
					list.Add(cg);
				}
				var objList2 = from Grades in context.StudentsMandatoryCourseGrade
							  join courses in context.MandatoryCourses on Grades.MandatoryCourse_Id equals courses.Id
							  join studs in context.Students on Grades.StudentId equals studs.Id
							  select new {
								  Grades,
								  CourseName = courses.Name,
								  StudentName = studs.Name
							  };
				foreach (var tmp in objList2) {
					CourseGradeViewModel cg = new CourseGradeViewModel();
					cg.Course_Id = tmp.Grades.MandatoryCourse_Id;
					cg.Grade = tmp.Grades.Grade;
					cg.Id = tmp.Grades.Id;
					cg.StudentId = tmp.Grades.StudentId;
					cg.Type = CourseType.Mandatory;
					cg.CourseName = tmp.CourseName;
					cg.StudentName = tmp.StudentName;
					list.Add(cg);
				}
				return list;
			}
		}

		public List<CourseGradeViewModel> GetCourseGradesByTeacher(String teachUserName, String courseName) {
			List<CourseGradeViewModel> list = new List<CourseGradeViewModel>();
			using (FacultyManagementContext context = new FacultyManagementContext()) {
				var objList = from Grades in context.StudentsFacultativeCourseGrade
							  join courses in context.FacultativeCourses on Grades.FacultativeCourse_Id equals courses.Id
							  join studs in context.Students on Grades.StudentId equals studs.Id
							  where courses.Name==courseName
							  select new {
								  Grades,
								  CourseName = courses.Name,
								  StudentName = studs.Name
							  };
				foreach (var tmp in objList) {
					CourseGradeViewModel cg = new CourseGradeViewModel();
					cg.Course_Id = tmp.Grades.FacultativeCourse_Id;
					cg.Grade = tmp.Grades.Grade;
					cg.Id = tmp.Grades.Id;
					cg.StudentId = tmp.Grades.StudentId;
					cg.Type = CourseType.Facultative;
					cg.CourseName = tmp.CourseName;
					cg.StudentName = tmp.StudentName;
					list.Add(cg);
				}
				var objList1 = from Grades in context.StudentsOptionalCourseGrade
							   join courses in context.OptionalCourses on Grades.OptionalCourse_Id equals courses.Id
							   join studs in context.Students on Grades.StudentId equals studs.Id
							   where courses.Name == courseName
							   select new {
								   Grades,
								   CourseName = courses.Name,
								   StudentName = studs.Name
							   };
				foreach (var tmp in objList1) {
					CourseGradeViewModel cg = new CourseGradeViewModel();
					cg.Course_Id = tmp.Grades.OptionalCourse_Id;
					cg.Grade = tmp.Grades.Grade;
					cg.Id = tmp.Grades.Id;
					cg.StudentId = tmp.Grades.StudentId;
					cg.Type = CourseType.Optional;
					cg.CourseName = tmp.CourseName;
					cg.StudentName = tmp.StudentName;
					list.Add(cg);
				}
				var objList2 = from Grades in context.StudentsMandatoryCourseGrade
							   join courses in context.MandatoryCourses on Grades.MandatoryCourse_Id equals courses.Id
							   join studs in context.Students on Grades.StudentId equals studs.Id
							   where courses.Name == courseName
							   select new {
								   Grades,
								   CourseName = courses.Name,
								   StudentName = studs.Name
							   };
				foreach (var tmp in objList2) {
					CourseGradeViewModel cg = new CourseGradeViewModel();
					cg.Course_Id = tmp.Grades.MandatoryCourse_Id;
					cg.Grade = tmp.Grades.Grade;
					cg.Id = tmp.Grades.Id;
					cg.StudentId = tmp.Grades.StudentId;
					cg.Type = CourseType.Mandatory;
					cg.CourseName = tmp.CourseName;
					cg.StudentName = tmp.StudentName;
					list.Add(cg);
				}
				return list;
			}
		}
    }
}