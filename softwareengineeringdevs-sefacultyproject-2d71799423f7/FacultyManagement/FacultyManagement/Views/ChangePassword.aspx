﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="FacultyManagement.Views.ChangePassword" %>

<!DOCTYPE html>

<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="resources/C1.css">
  <title>Recover</title>
</head>


<body>

    <img src="resources/Logo.png" id="logo">
    
	<form id="form" runat="server">
		<label id="Recover_Label" type="Singin">Change</label>
		<br>
        <label id="Reset_user_Label" type="Username">Username</label>
		<br>
        <asp:TextBox ID="TextBoxUsername" runat="server" type="Username" name="User" Font-Names="Comic Sans MS" Font-Size="Medium"></asp:TextBox>
		<br>
		<label id="Reset_code_Label" type="Username">Reset Code</label>
		<br>
        <asp:TextBox ID="TextBoxResetCode" runat="server" type="Username" name="User" Font-Names="Comic Sans MS" Font-Size="Medium"></asp:TextBox>
		<br>
		<label id="Reset_pass_Label" type="Password">Password</label>
		<br>
        <asp:TextBox ID="TextBoxPassword" runat="server" type="Password" name="Password" Font-Names="Comic Sans MS" Font-Size="Medium"></asp:TextBox>
        <br>
        <label id="Reset_confpass_Label" type="Password">Confirm Password</label>
		<br>
        <asp:TextBox ID="TextBoxConfirmPassword" runat="server" type="Password" name="ConfirmPassword" Font-Names="Comic Sans MS" Font-Size="Medium"></asp:TextBox>
        <br>
        <br>
        <asp:DropDownList ID="Type_Select" runat="server" BackColor="#E4D5C3" Width="300px" Font-Names="Comic Sans MS">
            <asp:ListItem>Student</asp:ListItem>
            <asp:ListItem>Teacher</asp:ListItem>
            <asp:ListItem>AdministrativeStaff</asp:ListItem>
            <asp:ListItem>ChiefOfDepartment</asp:ListItem>
        </asp:DropDownList>
        <br>
        <br>
        <asp:Button ID="ChangeButton" runat="server" class="submit-button" type="submit" Text="Submit" OnClick="ChangeButton_Click" Font-Names="Comic Sans MS" />
        <br>
        <a href="RecoverPassword.aspx">Recover Password</a>
	</form>

</body>
