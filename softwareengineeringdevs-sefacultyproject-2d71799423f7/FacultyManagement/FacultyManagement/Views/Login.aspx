﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="FacultyManagement.Views.Login" %>

<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="resources/C1.css">
  <title>Login</title>
</head>


<body>

    <img src="resources/Logo.png" id="logo">
    
	<form id="form" runat="server">
		<label id="Singing_Label" type="Singin">SingIn</label>
		<br>
		<label id="Username_Labe" type="Username">User</label>
		<br>
        <asp:TextBox ID="Username_TextBox" runat="server" type="Username" name="User" Font-Names="Comic Sans MS" Font-Size="Medium"></asp:TextBox>
		<br>
		<label id="Password_Label" type="Password">Pass</label>
		<br>
        <asp:TextBox ID="Password_TextBox" runat="server" type="Password" name="Pass" Font-Names="Arial" Font-Size="Medium"></asp:TextBox>
		<br>
        <br>
        <asp:DropDownList ID="Type_Select" runat="server" BackColor="#E4D5C3" Width="300px" Font-Names="Comic Sans MS">
            <asp:ListItem>Student</asp:ListItem>
            <asp:ListItem>Teacher</asp:ListItem>
            <asp:ListItem>AdministrativeStaff</asp:ListItem>
            <asp:ListItem>ChiefOfDepartment</asp:ListItem>
        </asp:DropDownList>
        <br>
        <br>
        <asp:Button ID="LoginButton" runat="server" class="submit-button" type="submit" Text="Submit" OnClick="LoginButton_Click" Font-Names="Comic Sans MS" />
        <br>
        <br>
        <a href="RecoverPassword.aspx">Recover Password</a>
	</form>

</body>

</html>
