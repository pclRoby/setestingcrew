
$(document).ready(function () {


	parallax.add($("#add_user"))
			.add($("#index"))
			.add($("#delete_user"))
			.add($("#user_details"))
			.add($("#edit_user"));

	parallax.background = $("body");

	//Clears each page navigation on load
	parallax.preload = function(){
		rightKey = leftKey = topKey = bottomKey = "";
		$(".control").hide();
	};


	//Setting up page navigation
	parallax.index.onload=function(){
		setCreate("add_user", "Create");
		setEdit("edit_user", "Edit");
		setDelete("delete_user", "Delete");
		setDetails("user_details", "Details");
	};
	
	
	
	function setCreate(page, text){
		$("#CreateControl").show().unbind('click').click(function(){
			parallax[page].left();
		});
	}

	function setDelete(page, text){
		$("#DeleteControl").show().unbind('click').click(function(){
			parallax[page].left();
		});
	}

	function setDetails(page, text){
		$("#DetailsControl").show().unbind('click').click(function(){
			parallax[page].left();
		});
	}
	
	function setEdit(page, text){
		$("#EditControl").show().unbind('click').click(function(){
			parallax[page].left();
		});
	}
	
	
	$(".control").hide();
	parallax.index.show();

	document.getElementById("body_di").scrollTop = 0;

});
