﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;
using FacultyManagement.Models.YearOfStudy;
using FacultyManagement.Models.StudyLevel;
using FacultyManagement.Models.Faculty;

namespace FacultyManagement.Views
{
    public partial class ChiefOfDepartment : System.Web.UI.Page
    {
        private ViewHomeController homeCtrl = new ViewHomeController();
        private ViewChiefOfDepartmentController codCtrl = new ViewChiefOfDepartmentController();
        protected void Page_Load(object sender, EventArgs e)
        {
			if (Session["New"] != null) {
				string Type = (string)Session["New"];
				Type = Type.Split('.')[0];
				if (Type == "ChiefOfDepartment") {
					;
				}
				else {
					Response.Redirect(Type + ".aspx");
				}
			}
			else {
				Response.Redirect("Login.aspx");
			}
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            TeacherViewModel var = new TeacherViewModel();

            var.Name = TeacherAddName.Text;
            var.Department = new Department();
            var.Department.Name = TeacherAddDep.Text;
            var.Username = TeacherAddUsername.Text;
            var.Password = TeacherAddPassword.Text;

            if (TeacherAddPassword.Text == TeacherAddPassword.Text && TeacherAddPassword.Text != string.Empty)
            {
                if (codCtrl.Create(var))
                {
                    LabelAddInteract.Text = "Teacherul a fost adaugat";
                }
                else
                {
                    LabelAddInteract.Text = "Eroare, daca persista contactati administratorul";
                }
            }
            else
            {
                LabelAddInteract.Text = "Parolele nu corespund";
            }
        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            TeacherViewModel var = new TeacherViewModel();

            string username = TeacherEditUsername.Text;

            var.Name = TeacherEditNewName.Text;
            var.Department = new Department();
            var.Department.Name = TeacherEditNewDep.Text;
            var.Username = TeacherEditNewUsername.Text;

            if (codCtrl.Edit(username, var))
            {
                LabelEditInteract.Text = "Teacherul a fost modificat";
            }
            else
            {
                LabelEditInteract.Text = "Eroare, verifica daca datele sunt valide - daca persista contactati administratorul";
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (TeacherDeleteUsername.Text == TeacherDeleteConfirmUsername.Text)
            {
                if (codCtrl.Delete(TeacherDeleteUsername.Text))
                {
                    LabelDeleteInteract.Text = "Userul cu idul " + TeacherDeleteUsername.Text + " a fost sters";
                }
                else
                {
                    LabelDeleteInteract.Text = "Userul cu idul " + TeacherDeleteUsername.Text + " nu a fost sters, probabil ca nu exista";
                }
            }
            else
            {
                LabelDeleteInteract.Text = "Idurile introduse nu corespund";
            }
        }

        protected void ButtonDetails_Click(object sender, EventArgs e)
        {
            TeacherViewModel var = new TeacherViewModel();

            var = codCtrl.Details(TeacherDetailsUsername.Text);

            if (var != null)
            {
                TeacherDetailsReadUser.Text = var.Username;
                TeacherDetailsReadName.Text = var.Name;
                TeacherDetailsReadDepName.Text = var.Department.Name;

            }
        }

        protected void ButtonSignout_Click(object sender, EventArgs e)
        {
            Session.Remove("New");
            Session.RemoveAll();
            Response.Redirect("Login.aspx");
        }
    }
}