﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Views
{
    public partial class Login : System.Web.UI.Page
    {
        private ViewHomeController homeCtrl = new ViewHomeController();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            LoginViewModel var = new LoginViewModel();

            string user = Username_TextBox.Text;
            string pass = Password_TextBox.Text;
            UserType type;
            Enum.TryParse(Type_Select.SelectedItem.Text, out type);

            var.Username = user;
            var.Password = pass;
            var.TypeOfUser = type;

            //am modificat magariile alea si nu mai merge nah lasa ca verific, mersi !!
            // cu placere :) apropo, eu cum pot vedea? ca am fisierele, ma pun sa schimb ce mi-ai zis 
            //si iti trimit dupa ok
            // no spor , si noapte buna mersi la fel!!

            if (homeCtrl.Login(var) == true)
            {
                Session["New"] = type.ToString()+"."+user+".";
                Response.Redirect(type.ToString() + ".aspx");
            }
            else
            {
                Username_TextBox.Text = "Wrong data input";
            }
        }

    }
}