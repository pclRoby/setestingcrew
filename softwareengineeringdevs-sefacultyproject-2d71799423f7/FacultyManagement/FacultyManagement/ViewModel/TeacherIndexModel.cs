﻿using System.Collections.Generic;

namespace FacultyManagement.ViewModel
{
	public class TeacherIndexModel
	{
		public List<TeacherViewModel> Teachers { get; set; }
	}
}