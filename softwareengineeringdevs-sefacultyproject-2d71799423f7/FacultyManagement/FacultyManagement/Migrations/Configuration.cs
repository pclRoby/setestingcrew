using System.Collections.Generic;
using System.Data.Entity.Migrations;
using FacultyManagement.Models;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.Faculty;
using FacultyManagement.Models.StudyLevel;
using FacultyManagement.Models.Users;
using FacultyManagement.Models.YearOfStudy;
using FacultyManagement.Services;

namespace FacultyManagement.Migrations
{
    public class Configuration : DbMigrationsConfiguration<FacultyManagementContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

		protected override void Seed(FacultyManagement.Models.FacultyManagementContext context) {

		}

        public static void SeedB(FacultyManagement.Models.FacultyManagementContext context)
        {
			// Admin staff
			var forestAdmin = new AdminStaff { Name = "forest", Username = "forest", Password = LoginService.EncodePassword("123") };

			// COD
			var istvanCOD = new ChiefOfDepartment { Name = "istvan", Username = "istvan", Password = LoginService.EncodePassword("456") };
			var florinCOD = new ChiefOfDepartment { Name = "florin", Username = "florin", Password = LoginService.EncodePassword("456") };

			// Teachers
			var forestTeacher = new Teacher { Name = "forest", Username = "forest", Password = LoginService.EncodePassword("123") };
			var braduTeacher = new Teacher { Name = "bradu", Username = "bradu", Password = LoginService.EncodePassword("123") };
			var dadiTeacher = new Teacher { Name = "dadi", Username = "dadi", Password = LoginService.EncodePassword("123") };
			var tzutzuTeacher = new Teacher { Name = "tzutzu", Username = "tzutzu", Password = LoginService.EncodePassword("123") };

			context.Teachers.AddOrUpdate(forestTeacher);
			context.Teachers.AddOrUpdate(braduTeacher);
			context.Teachers.AddOrUpdate(dadiTeacher);
			context.Teachers.AddOrUpdate(tzutzuTeacher);

			// Students
			var jack = new Student { Name = "jack", Username = "jack", Password = LoginService.EncodePassword("123") };
			var john = new Student { Name = "john", Username = "john", Password = LoginService.EncodePassword("123") };
			var tom = new Student { Name = "tom", Username = "tom", Password = LoginService.EncodePassword("123") };
			var jane = new Student { Name = "jane", Username = "jane", Password = LoginService.EncodePassword("123") };
			var anne = new Student { Name = "anne", Username = "anne", Password = LoginService.EncodePassword("123") };
			var mary = new Student { Name = "mary", Username = "anne", Password = LoginService.EncodePassword("123") };

			context.Students.AddOrUpdate(jack);
			context.Students.AddOrUpdate(john);
			context.Students.AddOrUpdate(tom);
			context.Students.AddOrUpdate(jane);
			context.Students.AddOrUpdate(anne);
			context.Students.AddOrUpdate(mary);

			// Student groups
			var s921 = new StudentsGroup { Name = "921", Students = new List<Student> { jack, jane } };
			var s922 = new StudentsGroup { Name = "922", Students = new List<Student> { john, anne } };
			var s923 = new StudentsGroup { Name = "923", Students = new List<Student> { tom, mary } };

			context.StudentsGroups.AddOrUpdate(s921);
			context.StudentsGroups.AddOrUpdate(s922);
			context.StudentsGroups.AddOrUpdate(s923);

			// Mandatory courses
			var fp = new MandatoryCourse { Name = "FP", Credits = 6 };
			var oop = new MandatoryCourse { Name = "OOP", Credits = 6 };
			var se = new MandatoryCourse { Name = "SE", Credits = 9 };

			context.MandatoryCourses.AddOrUpdate(fp);
			context.MandatoryCourses.AddOrUpdate(oop);
			context.MandatoryCourses.AddOrUpdate(se);

			// Optional courses
			var aop = new OptionalCourse { Name = "AOP", Credits = 6 };
			var vr = new OptionalCourse { Name = "VR", Credits = 6 };
			var cryptography = new OptionalCourse { Name = "Cryptography", Credits = 9 };

			context.OptionalCourses.AddOrUpdate(aop);
			context.OptionalCourses.AddOrUpdate(vr);
			context.OptionalCourses.AddOrUpdate(cryptography);

			// Facultative courses
			var ddd = new FacultativeCourse { Name = "ddd", Credits = 6 };
			var qwerty = new FacultativeCourse { Name = "qwerty", Credits = 6 };
			var latex = new FacultativeCourse { Name = "LaTEX", Credits = 9 };

			context.FacultativeCourses.AddOrUpdate(ddd);
			context.FacultativeCourses.AddOrUpdate(qwerty);
			context.FacultativeCourses.AddOrUpdate(latex);

			// Semesters
			var firstSemester = new Semester { Number = 1, Courses = new List<ICourse> { oop, fp, aop, vr, latex } };
			var secondSemester = new Semester { Number = 2, Courses = new List<ICourse> { se, cryptography, ddd, qwerty } };

			context.Semesters.AddOrUpdate(firstSemester);
			context.Semesters.AddOrUpdate(secondSemester);

			// Years of study
			var firstYear = new YearOfStudy { Name = "1", Semesters = new List<Semester> { firstSemester }, StudentsGroups = new List<StudentsGroup> { s921, s922 } };
			var secondYear = new YearOfStudy { Name = "2", Semesters = new List<Semester> { firstSemester }, StudentsGroups = new List<StudentsGroup> { s923 } };

			context.YearsOfStudy.AddOrUpdate(firstYear);
			context.YearsOfStudy.AddOrUpdate(secondYear);

			// Study languages
			var english = new StudyLanguage { Name = "english", YearsOfStudy = new List<YearOfStudy> { firstYear, secondYear } };
			var german = new StudyLanguage { Name = "german", YearsOfStudy = new List<YearOfStudy> { firstYear, secondYear } };
			var romanian = new StudyLanguage { Name = "romanian", YearsOfStudy = new List<YearOfStudy> { firstYear, secondYear } };

			context.StudyLanguages.AddOrUpdate(english);
			context.StudyLanguages.AddOrUpdate(german);
			context.StudyLanguages.AddOrUpdate(romanian);

			// Specialisations
			var softwareEngineering = new Specialisation { Name = "software engineering english", StudyLanguages = new List<StudyLanguage> { english, romanian } };
			var databases = new Specialisation { Name = "databases english", StudyLanguages = new List<StudyLanguage> { english } };
			var bigDataAnalysis = new Specialisation { Name = "big data analysis", StudyLanguages = new List<StudyLanguage> { german } };
			var cs = new Specialisation { Name = "computer science english", StudyLanguages = new List<StudyLanguage> { english } };
			var mathematics = new Specialisation { Name = "mathematics german", StudyLanguages = new List<StudyLanguage> { german } };

			context.Specialisations.AddOrUpdate(softwareEngineering);
			context.Specialisations.AddOrUpdate(databases);
			context.Specialisations.AddOrUpdate(bigDataAnalysis);
			context.Specialisations.AddOrUpdate(cs);
			context.Specialisations.AddOrUpdate(mathematics);

			// Graduate levels
			var masterEn = new Graduate { Name = "master english", Specialisations = new List<Specialisation> { softwareEngineering, databases } };
			var masterGer = new Graduate { Name = "master german", Specialisations = new List<Specialisation> { bigDataAnalysis } };
			var masterRo = new Graduate { Name = "master romanian", Specialisations = new List<Specialisation> { softwareEngineering } };

			context.GraduateLevels.AddOrUpdate(masterEn);
			context.GraduateLevels.AddOrUpdate(masterGer);
			context.GraduateLevels.AddOrUpdate(masterRo);

			// Undergraduate levels
			var licenceEn = new Undergraduate { Name = "licence english", Specialisations = new List<Specialisation> { cs } };
			var licenceGer = new Undergraduate { Name = "licence german", Specialisations = new List<Specialisation> { cs, mathematics } };
			var licenceRo = new Undergraduate { Name = "licence romanian", Specialisations = new List<Specialisation> { cs } };

			context.UndergraduateLevels.AddOrUpdate(licenceEn);
			context.UndergraduateLevels.AddOrUpdate(licenceGer);
			context.UndergraduateLevels.AddOrUpdate(licenceRo);

			// Departments
			var computerScienceDep = new Department { Name = "computer science", GraduateLevels = new List<Graduate> { masterEn }, UndergraduateLevels = new List<Undergraduate> { licenceEn }, Teachers = new List<Teacher> { forestTeacher, braduTeacher, tzutzuTeacher }, ChiefOfDepartment = istvanCOD };
			var mathematicsDep = new Department { Name = "mathematics", GraduateLevels = new List<Graduate>(), UndergraduateLevels = new List<Undergraduate>(), Teachers = new List<Teacher> { dadiTeacher, tzutzuTeacher }, ChiefOfDepartment = florinCOD };

			context.Departments.AddOrUpdate(computerScienceDep);
			context.Departments.AddOrUpdate(mathematicsDep);

			context.AdminStaves.AddOrUpdate(forestAdmin);
			context.ChiefsOfDepartments.AddOrUpdate(istvanCOD);
			context.ChiefsOfDepartments.AddOrUpdate(florinCOD);

			// Faculties 
			var computerScienceFaculty = new Faculty { Name = "mathematics & computer science", Departments = new List<Department> { computerScienceDep, mathematicsDep } };

			context.Faculties.AddOrUpdate(computerScienceFaculty);

			context.SaveChanges();
        }
    }
}
