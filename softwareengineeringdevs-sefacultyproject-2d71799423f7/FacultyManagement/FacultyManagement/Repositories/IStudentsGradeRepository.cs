﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.Users;

namespace FacultyManagement.Repositories
{
    interface IStudentsGradeRepository<StudentGrade>
    {
        void Add(StudentGrade studentGrade);

        void Update(StudentGrade studentGrade);

        void Delete(int id);

        StudentGrade FindById(int id);

        List<ICourse> FindStudentCourse(int studentId);

        List<Student> FindCourseStudents(int courseId);

        int GetStudentGrade(int studentId, int courseId);
    }
}
