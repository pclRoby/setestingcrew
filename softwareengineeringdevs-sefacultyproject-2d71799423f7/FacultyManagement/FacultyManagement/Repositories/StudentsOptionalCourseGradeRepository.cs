﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.StudentsGrade;
using FacultyManagement.Models;
using FacultyManagement.Models.Users;
using System.Data.Entity.Migrations;


namespace FacultyManagement.Repositories
{
    public class StudentsOptionalCourseGradeRepository : IStudentsGradeRepository<StudentsOptionalCourseGrade>
    {
        public void Add(StudentsOptionalCourseGrade studentGrade)
        {
            using (var context = new FacultyManagementContext())
            {
                context.StudentsOptionalCourseGrade.AddOrUpdate(studentGrade);
                context.SaveChanges();

            }
        }

		public StudentsOptionalCourseGrade FindWithoutID(int studID, int courseID) {
			using (var context = new FacultyManagementContext()) {
				return context.StudentsOptionalCourseGrade.Where(s => s.OptionalCourse_Id == 2 && s.StudentId == studID).FirstOrDefault();
			}
		}

        public void Delete(int id)
        {
            using (var context = new FacultyManagementContext())
            {
                var userGrade = this.FindById(id);
                if (userGrade == null)
                {
                    throw new KeyNotFoundException("User not found!");
                }
                context.StudentsOptionalCourseGrade.Remove(userGrade);
                context.SaveChanges();
            }
        }


        public void Update(StudentsOptionalCourseGrade userGrade)
        {
            using (var context = new FacultyManagementContext())
            {
                context.StudentsOptionalCourseGrade.AddOrUpdate(userGrade);
                context.SaveChanges();
            }
        }


        public StudentsOptionalCourseGrade FindById(int id)
        {
            using (var context = new FacultyManagementContext())
            {
                return context.StudentsOptionalCourseGrade.Find(id);
            }
        }

        public List<ICourse> FindStudentCourse(int studentId)
        {
            List<ICourse> list = new List<ICourse>();
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsOptionalCourseGrade studentGrade in context.StudentsOptionalCourseGrade.ToList())
                {
                    if (studentGrade.StudentId == studentId)
                        list.Add(context.OptionalCourses.Find(studentGrade.OptionalCourse_Id));
                }
            }
            return list;
        }

        public List<Student> FindCourseStudents(int courseId)
        {
            List<Student> list = new List<Student>();
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsOptionalCourseGrade studentGrade in context.StudentsOptionalCourseGrade.ToList())
                {
                    if (studentGrade.OptionalCourse_Id == courseId)
                        list.Add(context.Students.Find(studentGrade.StudentId));
                }
            }
            return list;
        }

        public int GetStudentGrade(int studentId, int courseId)
        {
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsOptionalCourseGrade studentGrade in context.StudentsOptionalCourseGrade.ToList())
                    if (studentGrade.StudentId == studentId && studentGrade.OptionalCourse_Id == courseId)
                        return studentGrade.Grade;
                return -1;
            }
        }
    }
}