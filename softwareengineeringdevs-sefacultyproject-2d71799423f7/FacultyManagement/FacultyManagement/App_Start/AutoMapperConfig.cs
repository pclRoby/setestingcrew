﻿using System;
using System.Reflection;
using AutoAutoMapper;
using AutoMapper;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.Users;
using FacultyManagement.ViewModel;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using FacultyManagement.Models.Faculty;
using FacultyManagement.Models.YearOfStudy;

namespace FacultyManagement.App_Start
{
    public static class AutoMapperConfig
    {
		public static void CreateMaps()
		{
			Mapper.CreateMap<IUser, LoginViewModel>();
			Mapper.CreateMap<LoginViewModel, IUser>();

			Mapper.CreateMap<Student, StudentViewModel>();
			Mapper.CreateMap<StudentViewModel, Student>();

			Mapper.CreateMap<Teacher, TeacherViewModel>();
			Mapper.CreateMap<TeacherViewModel, Teacher>();
		}
    }
}
