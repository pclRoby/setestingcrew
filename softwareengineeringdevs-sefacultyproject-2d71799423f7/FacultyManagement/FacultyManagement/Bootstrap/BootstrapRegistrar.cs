﻿using Autofac;
using Autofac.Integration.Mvc;

namespace FacultyManagement.Bootstrap
{
	public class BootstrapRegistrar
	{
		public static IContainer BuildContainer()
		{
			var builder = new ContainerBuilder();

			builder.RegisterControllers(typeof(MvcApplication).Assembly);

			ServicesRegistrar.RegisterWith(builder);
			RepositoryRegistrar.RegisterWith(builder);

			var container = builder.Build();

			return container;
		}
	}
}