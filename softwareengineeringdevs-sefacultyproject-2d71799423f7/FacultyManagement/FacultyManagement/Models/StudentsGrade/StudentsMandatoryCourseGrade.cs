﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacultyManagement.Models.StudentsGrade;

namespace FacultyManagement.Models.StudentsGrade
{
    public class StudentsMandatoryCourseGrade:IStudentsGrade
    {
        public int Id { set; get; }

        public int StudentId { set; get; }

        public int Grade { set; get; }

        public int MandatoryCourse_Id { set; get; }
    }
}