﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.StudyLevel;
using FacultyManagement.Models.Users;

namespace FacultyManagement.Models.Faculty
{
	public class Department
	{
		[Key]
		public int Id { get; set; }
		
		[Required]
		public string Name { get; set; }

		public virtual ChiefOfDepartment ChiefOfDepartment { get; set; }

		public virtual ICollection<Teacher> Teachers { get; set; }

		public virtual ICollection<Graduate> GraduateLevels { get; set; }

		public virtual ICollection<Undergraduate> UndergraduateLevels { get; set; } 

		public virtual Faculty Faculty { get; set; }
	}
}