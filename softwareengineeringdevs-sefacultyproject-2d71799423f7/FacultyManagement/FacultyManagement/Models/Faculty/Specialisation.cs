﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.StudyLevel;

namespace FacultyManagement.Models.Faculty
{
	public class Specialisation
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		public virtual ICollection<StudyLanguage> StudyLanguages { get; set; }

		public virtual ICollection<IStudyLevel> StudyLevels { get; set; }
	}
}