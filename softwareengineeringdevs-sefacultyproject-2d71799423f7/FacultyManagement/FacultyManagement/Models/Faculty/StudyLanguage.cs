﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.Models.Faculty
{
	public class StudyLanguage
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		public virtual ICollection<YearOfStudy.YearOfStudy> YearsOfStudy { get; set; }

		public virtual ICollection<Specialisation> Specialisations { get; set; }
	}
}