﻿using System.Data.Entity;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.Faculty;
using FacultyManagement.Models.StudyLevel;
using FacultyManagement.Models.Users;
using FacultyManagement.Models.YearOfStudy;
using FacultyManagement.Models.StudentsGrade;

namespace FacultyManagement.Models
{
	public class FacultyManagementContext : DbContext
	{
		public FacultyManagementContext()
			: base("DefaultConnection")
		{
		}

        public DbSet<StudentsFacultativeCourseGrade> StudentsFacultativeCourseGrade { get; set; }
        public DbSet<StudentsMandatoryCourseGrade> StudentsMandatoryCourseGrade { get; set; }
        public DbSet<StudentsOptionalCourseGrade> StudentsOptionalCourseGrade { get; set; }
		public DbSet<FacultativeCourse> FacultativeCourses { get; set; }
		public DbSet<MandatoryCourse> MandatoryCourses { get; set; }
		public DbSet<OptionalCourse> OptionalCourses { get; set; }
		public DbSet<Department> Departments { get; set; }
		public DbSet<Faculty.Faculty> Faculties { get; set; }
		public DbSet<Specialisation> Specialisations { get; set; }
		public DbSet<StudyLanguage> StudyLanguages { get; set; }
		public DbSet<Graduate> GraduateLevels { get; set; }
		public DbSet<Undergraduate> UndergraduateLevels { get; set; }
		public DbSet<AdminStaff> AdminStaves { get; set; }
		public DbSet<ChiefOfDepartment> ChiefsOfDepartments { get; set; }
		public DbSet<Student> Students { get; set; }
		public DbSet<Teacher> Teachers { get; set; }
		public DbSet<Semester> Semesters { get; set; }
		public DbSet<StudentsGroup> StudentsGroups { get; set; }
		public DbSet<YearOfStudy.YearOfStudy> YearsOfStudy { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Department>()
				.HasMany(dep => dep.GraduateLevels)
				.WithOptional(grad => grad.Department);
			modelBuilder.Entity<Department>()
				.HasMany(dep => dep.UndergraduateLevels)
				.WithOptional(under => under.Department);
			modelBuilder.Entity<Department>()
				.HasRequired(dep => dep.ChiefOfDepartment)
				.WithOptional(cod => cod.CDepartment);
			base.OnModelCreating(modelBuilder);
		}
	}
}
