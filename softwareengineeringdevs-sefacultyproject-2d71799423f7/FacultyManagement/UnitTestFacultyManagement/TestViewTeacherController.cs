﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;
using FacultyManagement.Services;
using FacultyManagement.Repositories;
using FacultyManagement.Models.StudentsGrade;

namespace UnitTestFacultyManagement
{
    [TestClass]
    public class TestViewTeacherController : Controller
    {
        private ViewTeacherController ctrl;
        private readonly StudentsFacultativeCourseGradeRepository facultativeGrades = new StudentsFacultativeCourseGradeRepository();
        private readonly StudentsMandatoryCourseGradeRepository mandatoryGrades = new StudentsMandatoryCourseGradeRepository();
        private readonly StudentsOptionalCourseGradeRepository optionalGrades = new StudentsOptionalCourseGradeRepository();

        [TestInitialize]
        public void TestControllerSetUp()
        {
            FacultyManagement.App_Start.AutoMapperConfig.CreateMaps();
            this.ctrl = new ViewTeacherController();
        }

        [TestMethod]
        public void TestControllerUpdate()
        {
            CourseGradeViewModel grade1 = new CourseGradeViewModel { StudentId = 1, Course_Id = 1, Grade = 10, Type = CourseType.Mandatory };
            this.ctrl.UpdateGrade(grade1);
            StudentsMandatoryCourseGrade gr1 = mandatoryGrades.FindWithoutID(grade1.StudentId, grade1.Course_Id);
            Assert.IsTrue(gr1.Grade.Equals(grade1.Grade), "Mandatory Course Grade not updated correctly");

            CourseGradeViewModel grade2 = new CourseGradeViewModel { StudentId = 2, Course_Id = 2, Grade = 10, Type = CourseType.Facultative };
            this.ctrl.UpdateGrade(grade2);
            StudentsFacultativeCourseGrade gr2 = facultativeGrades.FindWithoutID(grade2.StudentId, grade2.Course_Id);
            Assert.IsTrue(gr1.Grade.Equals(grade2.Grade), "Facultative Course Grade not updated correctly");

            CourseGradeViewModel grade3 = new CourseGradeViewModel { StudentId = 3, Course_Id = 3, Grade = 10, Type = CourseType.Optional };
            this.ctrl.UpdateGrade(grade3);
            StudentsOptionalCourseGrade gr3 = optionalGrades.FindWithoutID(grade3.StudentId, grade3.Course_Id);
            Assert.IsTrue(gr1.Grade.Equals(grade3.Grade), "Optional Course Grade not updated correctly");
        }

    }
}
